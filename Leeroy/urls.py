"""Leeroy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', 'base.views.index',name='index'),
    url(r'^auth/$','base.views.auth',name='auth'),
    url(r'^auth/login/$', 'base.views.user_login',name='user_login'),
    url(r'^auth/register/$','base.views.user_register', name='user_register'),
    url(r'^auth/logout/$','base.views.user_logout', name='user_logout'),
    url(r'^list/$','base.views.get_list',name='get_list'),
    url(r'^entry/add','base.views.entry_add',name='entry_add'),
    url(r'^comment/add/$','base.views.add_comment',name='add_comment'),
    url(r'^entry/(?P<entry_id>\d+)/$','base.views.get_entry',name='get_entry'),
    url(r'^mistakes/add/$','base.views.add_mistake',name='add_mistake'),
    url(r'^comment/like/$','base.views.comment_like',name='comment_like'),
    url(r'^user/(?P<user_id>\d+)/$','base.views.get_user',name='get_user'),

]
