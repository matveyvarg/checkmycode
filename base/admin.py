from django.contrib import admin
from base.models import *
admin.site.register(Entry)
admin.site.register(UserProfile)
admin.site.register(Mistake)
# Register your models here.
