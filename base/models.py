from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class UserProfile(models.Model):

    user = models.OneToOneField(User,related_name='user_profile')
    code_level = models.DecimalField(max_digits=3,decimal_places=2,default=0)
    profile_level = models.IntegerField(default=0)
    current_exp = models.IntegerField(default=0)
    next_lvl_exp = models.IntegerField(default=60)
    def __unicode__(self):
        return self.user.username

class Entry(models.Model):

    langs = (
        ('java','Java'),
        ('php','PHP'),
        ('python','Python'),
        ('ruby','Ruby'),
    )

    source = models.TextField(max_length=2000)
    post_type = models.BooleanField()
    descripton = models.TextField(max_length=500,default='')
    lang = models.CharField(choices=langs ,default='python',max_length=16)
    author = models.ForeignKey(UserProfile,related_name='entry',null=True)

    def __unicode__(self):
        return str(self.id)
class Comment(models.Model):

    author = models.ForeignKey(UserProfile,related_name='comment',null=True)
    entry = models.ForeignKey(Entry,related_name='comment',null=True)
    first_line = models.IntegerField(default=0)
    last_line = models.IntegerField(default=0)
    text = models.TextField(max_length=300)
    rating = models.IntegerField(default=0)

    def __unicode__(self):
        return self.author

class Mistake(models.Model):
    post = models.OneToOneField(Entry,related_name='mistakes')
    user = models.OneToOneField(UserProfile)
    first_line = models.IntegerField()
    last_line = models.IntegerField()
