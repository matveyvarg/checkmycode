def send_response(src):
    response = src
    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    response['Access-Control-Allow-Credentials'] = 'true'

    return response
