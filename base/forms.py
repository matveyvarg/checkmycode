from django import forms

from .models import *


class UserAuth(forms.ModelForm):
	class Meta:
		model = User
		fields = ['username','email','password']

	def is_valid(self):

		valid = super(UserAuth,self).is_valid()

		if not valid:
			return valid

		if self.cleaned_data['email'] == '':
			self._errors['email'] = []
			self._errors['email'].append('Empty email')
			return False

		if  self.cleaned_data['username'] == '':
			self._errors['username'] = []
			self._errors['username'].append('username is empty')
			return False

		if  User.objects.filter(email=self.cleaned_data['email']).exists():
			self._errors['username'] = []
			self._errors['username'].append('user already exists')
			return False


		return True
class UserLogin(forms.ModelForm):
	class Meta:
		model = User
		fields = ['email','password']

	def is_valid(self):

		valid = super(UserLogin,self).is_valid()
		if not valid:
			return valid

		if(self.cleaned_data['email']==''):
			self._errors['email'] = []
			self._errors['email'].append('Empty email')
			return False

		else:
			try:
				user = User.objects.get(email=self.cleaned_data['email'])
				print 'Clen'
			except:
				self._errors['email'] = []
				self._errors['email'].append('User does not exist')
				return False

		return True
class EntryAdd(forms.ModelForm):
    lang = forms.ChoiceField(choices=Entry.langs)
    class Meta:
        model = Entry
        fields = ['lang','post_type','source','descripton']

class CommentAdd(forms.ModelForm):
	class Meta:
		model = Comment
		fields = ['text']
