# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-14 07:19
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0003_entry_author'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_line', models.IntegerField(default=0)),
                ('last_line', models.IntegerField(default=0)),
                ('text', models.TextField(max_length=300)),
                ('rating', models.IntegerField(default=0)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comment', to='base.UserProfile')),
            ],
        ),
        migrations.AlterField(
            model_name='entry',
            name='source',
            field=models.TextField(max_length=2000),
        ),
        migrations.AddField(
            model_name='comment',
            name='entry',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comment', to='base.Entry'),
        ),
    ]
