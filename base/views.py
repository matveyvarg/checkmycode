from django.shortcuts import render
from base.forms import *
from django.template import RequestContext
from django.shortcuts import render, render_to_response
from django.contrib.auth.models import User
from base.models import *
from django.contrib.auth import authenticate, login, logout
import hashlib
import json
from django.http import HttpResponse, HttpResponseRedirect
from django.core import serializers
from django.contrib.auth.decorators import login_required
import response

from level_count import add_exp

from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter

from collections import defaultdict

def get_user_info(user):
    data = {}

    data['email'] = hashlib.md5(user.email).hexdigest()
    data['name'] = user.username
    data['id'] =user.id

    return data

def get_entry_info(entry):

    data = {}

    data['email'] = hashlib.md5(user.email).hexdigest()
    data['name'] = user.username
    data['id'] =user.id

    return data

# Create your views here.
def index(request):

    context = RequestContext(request)

    response = render_to_response('index.html',context=context)
    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    response['Access-Control-Allow-Credentials'] = 'true'

    return response
def user_register(request):

    context = RequestContext(request)

    data = {}

    data['data'] = []

    if request.method == 'POST':

        userform = UserAuth(data = request.POST)
        try:
            username = request.POST['username']
            password = request.POST['password']
        except:
            response = HttpResponse(json.dumps({'status':'error','data':{'form':['Invalid username or password.']}}))
            response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
            response['Access-Control-Allow-Credentials'] = 'true'

            return response
        if userform.is_valid():

            user = userform.save()
            user.set_password(user.password)
            user.save()

            user = User.objects.get(username=username)
    #       print user.username
            user_profile = UserProfile(user=user)
            user_profile.save()

            user_auth = authenticate(username=username,password=password)

            if user_auth:

                if user_auth.is_active:

                    login(request,user_auth)
                    data = get_user_info(request.user)
                    response = HttpResponse(json.dumps({'status':'success','data':data}))
                    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
                    response['Access-Control-Allow-Credentials'] = 'true'

                    return response
        else:

            data['status'] = 'error'
            data['data'] = userform.errors

            response = HttpResponse(json.dumps(data))
            response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
            response['Access-Control-Allow-Credentials'] = 'true'

            return response
    else:

        userform = UserAuth()

    response =  render_to_response('register.html',{'form':userform},context)
    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    response['Access-Control-Allow-Credentials'] = 'true'

    return response

def auth(request):

    conetext = RequestContext(request)

    data = {}

    if request.user.is_authenticated():
        data['data'] = {}
        data['status'] = 'success'
        data['data']['email'] = hashlib.md5(request.user.email).hexdigest()
        data['data']['name'] = request.user.username
        data['data']['id'] = request.user.id
    else:
        data['status'] = 'error'
    response = HttpResponse(json.dumps(data))
    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    response['Access-Control-Allow-Credentials'] = 'true'

    return response

def user_login(request):

    context = RequestContext(request)

    if request.method == 'POST':

        data = {}
        data['data'] = {}
        form = UserLogin(data=request.POST)

        if form.is_valid():

            _user = User.objects.get(email=request.POST['email'])
            try:
                user = authenticate(username = _user.username, password=request.POST['password'])
                print 'check password  '
            except:
                print 'Wrong password'
                data['status'] = 'error'
                response = HttpResponse(json.dumps(data))
                response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
                response['Access-Control-Allow-Credentials'] = 'true'

                return response
            if user:
                if user.is_active:
                    login(request,user)
                    data['status'] = 'success'
                    data['data']['email'] = hashlib.md5(request.user.email).hexdigest()
                    data['data']['name'] = request.user.username
                    data['data']['id'] = request.user.id

                    response = HttpResponse(json.dumps(data))
                    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
                    response['Access-Control-Allow-Credentials'] = 'true'

                    return response
            else:
                data['status'] = 'error'
                data['data']= {'password':['Wrong password']}
                response = HttpResponse(json.dumps(data))
                response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
                response['Access-Control-Allow-Credentials'] = 'true'

                return response
        else:
            print 'Invalid form'
            data['status'] = 'error'
            data['data'] = form.errors
            response = HttpResponse(json.dumps(data))
            response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
            response['Access-Control-Allow-Credentials'] = 'true'
            return response
    else:
        response = render_to_response('login.html',context=context)
        response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
        response['Access-Control-Allow-Credentials'] = 'true'
        return response

def user_logout(request):

    logout(request)

    response = HttpResponse(json.dumps({'status':'success'}))
    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    response['Access-Control-Allow-Credentials'] = 'true'
    return response
def get_list(request):

    context = RequestContext(request)

    try:
        entry = Entry.objects.filter(lang=request.POST['lang'])
        print request.POST['lang']
    except:
        filt = ''
        entry = Entry.objects.filter(   )
    data = {}
    data['data'] = []

    for obj in entry :
        miss = defaultdict(int)
        mistakes = Mistake.objects.filter(post=obj)
        for item in mistakes:
            if item.first_line == item.last_line:
                miss[item.first_line] +=1
                continue
            miss[item.first_line] += 1
            miss[item.last_line] +=1
        data['data'].append({
            'id':obj.id,
            'source':obj.source,
            'descripton':obj.descripton,
            'post_type':obj.post_type,
            'author':obj.author.user.username,
            'comments':list(obj.comment.values()),
            'mistakes':miss


        })

    if len(data['data']) == 0:

        data['data'] = 'empty list'
        data['status'] = 'error'

    response = HttpResponse(json.dumps(data))
    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    response['Access-Control-Allow-Credentials'] = 'true'

    return response
@login_required
def entry_add(request):

    context = RequestContext(request)
    if request.method == 'POST':

        form = EntryAdd(request.POST)

        if form.is_valid():

            new_entry = form.save()

            new_entry.author = request.user.user_profile
            new_entry.save()

        response = HttpResponse(json.dumps({'status':'success'}))
        response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
        response['Access-Control-Allow-Credentials'] = 'true'
        return response
    else:
        form = EntryAdd()

    response = render_to_response('entry_add.html',{'form':form},context)
    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    response['Access-Control-Allow-Credentials'] = 'true'
    return response

@login_required
def add_comment(request):

    context = RequestContext(request)

    if request.method == 'POST':

        form = CommentAdd(request.POST)

        if form.is_valid():

            new_comment = form.save()
            new_comment.author = request.user.user_profile
            try:
                entry = Entry.objects.get(id=request.POST['id'])
            except:
                response(HttpResponse(json.dumps({'status':'error'})))
            new_comment.entry = entry
            new_comment.save()
            response = HttpResponse(json.dumps({'status':'success'}))
            response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
            response['Access-Control-Allow-Credentials'] = 'true'

            return  response
    else:
        form = CommentAdd()
        response = render_to_response('add_comment.html',{'form':form},context)
        response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
        response['Access-Control-Allow-Credentials'] = 'true'
        return response

def get_entry(request,entry_id):

    context = RequestContext(request)
    print entry_id
    try:
        entry = Entry.objects.get(id=entry_id)
    except:
        response = HttpResponse(json.dumps({'status':'error'}))
        response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
        response['Access-Control-Allow-Credentials'] = 'true'
        return response

    code = entry.source
    lexer = get_lexer_by_name(entry.lang,stripall=True)
    formater = HtmlFormatter(lineous=True,style='colorful')
    result =  highlight(code,lexer,formater)

    response = HttpResponse(json.dumps({'status':'success','data':result}))
    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    response['Access-Control-Allow-Credentials'] = 'true'
    return response

@login_required
def add_mistake(request):

 context = RequestContext(request)

 if request.method == 'POST':
     post = Entry.objects.get(id=request.POST['post_id'])
     mistake = Mistake(post=post)
     mistake.user = request.user.user_profile
     first_line = request.POST['first_line']
     last_line = request.POST['last_line']
     mistake.first_line = first_line
     mistake.last_line = last_line

     mistake.save()


     response = HttpResponse(json.dumps({'status':'success'}))
     response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
     response['Access-Control-Allow-Credentials'] = 'true'
     return response

@login_required
def comment_like(request):
    context = RequestContext(request)
    if request.method == 'POST':
        comment_id = request.POST['comment_id']
        comment = Comment.objects.get(id=comment_id)
        comment.rating +=1
        comment.save()

        if add_exp(comment.author):

            response = HttpResponse(json.dumps({'status':'success'}))
            response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
            response['Access-Control-Allow-Credentials'] = 'true'
            return response

        else:
            response = HttpResponse(json.dumps({'status':'error','data':{'add_exp':['Addig experience failed']}}))
            response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
            response['Access-Control-Allow-Credentials'] = 'true'
            return response
    else:
        response = render_to_response('like_com.html',context = context)
        response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
        response['Access-Control-Allow-Credentials'] = 'true'
        return response



def get_user(request,user_id):

    context = RequestContext(request)
    try:
        user = User.objects.get(id=user_id)
        data ={
            'id':user.id,
            'username':user.username,
            'email':hashlib.md5(user.email).hexdigest(),
            'profile_level':user.user_profile.profile_level,
            'current_exp':user.user_profile.current_exp,
        }
    except:
        data={'status':'error'}    
    response = HttpResponse(json.dumps(data))
    response['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    response['Access-Control-Allow-Credentials'] = 'true'
    return response
